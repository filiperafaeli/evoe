from django.contrib import messages
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.views import View
from rest_framework.authtoken.models import Token

from evoe.core import choices
from evoe.core.models import Lista
from evoe.core.services import TarefaService


def index(request):
    if not request.user.is_authenticated:
        return redirect('login')
    return redirect('todo_list')


@login_required()
def atualizar_tarefa(request, pk, acao):
    TarefaService().alterar_status(pk, request.user, acao)
    return redirect('todo_list')


@login_required()
def todo_list(request):
    lista = Lista.objects.ativos().por_usuario(request.user)
    ctx = {
        'tarefas_pendentes': lista.filter(status=choices.TAREFA_TODO),
        'tarefas_andamento': lista.filter(status=choices.TAREFA_DOING),
        'tarefas_concluidas': lista.filter(status=choices.TAREFA_DONE),
    }
    return render(request, 'core/index.html', ctx)


@login_required()
def gerar_token(request):
    obj, _ = Token.objects.get_or_create(user=request.user)
    key = obj.key
    ctx = {
        'token': key
    }
    return render(request, 'core/token.html', ctx)


def autenticar(request, usuario, senha):
    user = authenticate(request, username=usuario, password=senha)
    if user is not None:
        login(request, user)
        return redirect('index')
    else:
        messages.error(request, 'Dados inválidos')
        return render(request, 'user/login.html')


class Login(View):
    def get(self, request):
        return render(request, 'user/login.html')

    def post(self, request):
        if not request.user.is_authenticated:
            usuario = request.POST.get('user')
            senha = request.POST.get('pass')
            return autenticar(request, usuario, senha)


class Register(View):
    def get(self, request):
        return render(request, 'user/register.html')

    def post(self, request):
        if not request.user.is_authenticated:
            usuario = request.POST.get('user')
            senha = request.POST.get('pass')
            User.objects.create_user(username=usuario, password=senha)
            return autenticar(request, usuario, senha)
