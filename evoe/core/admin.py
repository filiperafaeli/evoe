from django.contrib import admin
from django.contrib.auth.models import Group
from rest_framework.authtoken.admin import TokenAdmin
from rest_framework.authtoken.models import Token


class AuthTokenAdmin(TokenAdmin):
    can_add_related = False

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        form.base_fields['user'].widget.can_add_related = False
        form.base_fields['user'].widget.can_change_related = False
        form.base_fields['user'].widget.can_delete_related = False
        return form


admin.site.unregister(Group)
admin.site.register(Token, AuthTokenAdmin)
