TAREFA_TODO = 'TODO'
TAREFA_DOING = 'DOING'
TAREFA_DONE = 'DONE'

OPCORES_TAREFA = [
    (TAREFA_TODO, 'Pendente'),
    (TAREFA_DOING, 'Em andamento'),
    (TAREFA_DONE, 'Concluída'),
]
