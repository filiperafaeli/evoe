import unittest

from django.contrib.auth.models import User

from evoe.core.models import Lista


class TestService(unittest.TestCase):
    def usuario(self):
        return User.objects.create_user(username='Filipe', password='123')

    def test_criar_tarefa(self, usuario):
        titulo = 'Teste de tarefa'
        descricao = 'Executar teste'
        resultado = Lista.objects.create(titulo=titulo, descricao=descricao, user=usuario)
        self.assertIsNotNone(resultado)
        self.assertEquals(resultado.titulo, titulo)
