import uuid

from django.contrib.auth.models import User
from django.db import models

from evoe.core import choices
from evoe.core.managers import BaseQuerySet


class BaseModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    criado_em = models.DateTimeField('criado em', auto_now_add=True)
    atualizado_em = models.DateTimeField('última atualização', auto_now=True)
    ativo = models.BooleanField(default=True)

    objects = BaseQuerySet().as_manager()

    class Meta:
        abstract = True


class Lista(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='tarefas')
    titulo = models.CharField('Título da tarefa', max_length=100)
    descricao = models.TextField('Descrição da tarefa')
    status = models.CharField('Status da tarefa', max_length=50, choices=choices.OPCORES_TAREFA, default=choices.TAREFA_TODO)
