from evoe.core.models import Lista


class TarefaService:
    def criar_nova_tarefa(self, titulo, descricao, user):
        return Lista.objects.create(titulo=titulo, descricao=descricao, user=user)

    def alterar_status(self, pk, user, status):
        tarefa = Lista.objects.get(id=pk)
        if tarefa.user != user:
            raise Exception('Vocẽ não pode alterar esta tarefa, ela não é sua.')
        tarefa.status = status
        tarefa.save()
        return tarefa
