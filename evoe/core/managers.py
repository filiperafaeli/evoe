from django.db import models


class BaseQuerySet(models.QuerySet):
    def ativos(self):
        return self.filter(ativo=True)

    def por_usuario(self, user):
        return self.filter(user=user)
