from django.urls import path

from evoe.api import views

api_urls = [
    path('api/v1/criar_tarefa/', views.CriarTarefa.as_view(), name='criar_tarefa'),
    path('api/v1/setar_status/', views.SetarStatus.as_view(), name='setar_status'),
]
