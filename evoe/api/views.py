from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from evoe.core.services import TarefaService


class CriarTarefa(APIView):
    """
        Endpoint para criar a tarefa
        Exemplo Json do post:
        {
            "titulo":"Lavar a roupa",
            "descricao":"pegar o monte de roupas sujas e por na maquina de lavar"
        }
        resultado json com id da tarefa e status 200 ok ou 400 com exception
    """

    permission_classes = (IsAuthenticated,)

    def post(self, request):
        try:
            user = request.user
            titulo = request.data.get('titulo')
            descricao = request.data.get('descricao')
            tarefa = TarefaService().criar_nova_tarefa(titulo, descricao, user)
            resultado = {
                'id_tarefa': tarefa.id.hex
            }
            return Response(resultado, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)


class ExecutarTarefa(APIView):
    """
        Endpoint para setar a tarefa como em execução
        Exemplo Json do post:
        {
            "id":"uuid_tarefa",
        }
        resultado status 200 ok ou 400 com exception
    """

    permission_classes = (IsAuthenticated,)

    def post(self, request):
        try:
            user = request.user
            tarefa_id = request.data.get('id')
            TarefaService().executar_tarefa(tarefa_id, user)
            return Response(status=status.HTTP_200_OK)
        except Exception as e:
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)


class SetarStatus(APIView):
    """
        Endpoint para setar a tarefa como concluída
        Exemplo Json do post:
        {
            "id":"uuid_tarefa",
            "status": "DOING"
        }
        opções de statu: 'TODO', 'DOING' e 'DONE
        resultado status 200 ok ou 400 com exception
    """

    permission_classes = (IsAuthenticated,)

    def post(self, request):
        try:
            user = request.user
            tarefa_id = request.data.get('id')
            status_tarefa = request.data.get('status')
            TarefaService().alterar_status(tarefa_id, user, status_tarefa)
            return Response(status=status.HTTP_200_OK)
        except Exception as e:
            return Response(str(e), status=status.HTTP_400_BAD_REQUEST)
