from django.contrib import admin
from django.urls import path, include

from evoe.api.urls import api_urls as api_v1_urls
from evoe.core import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', views.index, name='index'),
    path('tarefas/', views.todo_list, name='todo_list'),
    path('gerar_token/', views.gerar_token, name='gerar_token'),
    path('atualizar_tarefa/<pk>/<acao>/', views.atualizar_tarefa, name='atualizar_tarefa'),
    path('login/', views.Login.as_view(), name='login'),
    path('register/', views.Register.as_view(), name='register'),
]

urlpatterns += api_v1_urls
