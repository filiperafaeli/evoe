# ToDo List
Projeto de um sistema de tarefas, cadastrando tarefas por API e alterando status por api ou interface web

## Rodando o projeto
- baixe os codigos.
- instale os pacotes necessários executando o comando `pip install -r requirements.txt`
- execute a criação da base de dados com `python manage.py migrate`
- rode o sistema com `python manage.py runserver 0.0.0.0:8000`
- abra o link no navegador: http://127.0.0.1:8000/
- Faça seu cadastro
- na tela das tarefas, terá uma opção de obter token, nesta opção será gerado o token para acessar a API, on endpoints estão listado nesta tela 
  tambem.